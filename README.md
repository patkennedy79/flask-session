## Overview

This Flask application provides an example of how to use Sessions in Flask.

This project is utilized in the following blog post on [TestDriven.io](https://testdriven.io/blog/):

[Sessions in Flask](https://testdriven.io/blog/flask-sessions/)

## Installation Instructions

Pull down the source code from this GitLab repository:

```sh
git clone git@gitlab.com:patkennedy79/flask-session.git
```

Create a new virtual environment:

```sh
$ cd flask-session
$ python3 -m venv venv
```

Activate the virtual environment:

```sh
$ source venv/bin/activate
```

Install the python packages in requirements.txt:

```sh
(venv) $ pip install -r requirements.txt
```

## How to Run

In the top-level directory, set the file that contains the Flask application and specify that the development environment should be used:

```sh
(venv) $ export FLASK_APP=app.py
(venv) $ export FLASK_ENV=development
```

Run development server to serve the Flask application:

```sh
(venv) $ flask run
```

Navigate to 'http://localhost:5000/get_email' to view the website!

## Configuration

The following environment variable needs to be defined:

* SECRET_KEY - used to cryptographically-sign the cookies used for storing the session data

### Secret Key

The 'SECRET_KEY' can be generated using the following commands (assumes Python 3.6 or later):

```sh
(venv) $ python

>>> import secrets
>>> print(secrets.token_bytes(32))
>>> quit()

(venv) $ export SECRET_KEY=<secret_key_generated_in_interpreter>
```

NOTE: If working on Windows, use `set` instead of `export`.

## Key Python Modules Used

- Flask: micro-framework for web application development

This application is written using Python 3.9.2.
